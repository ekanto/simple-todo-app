import React, { Component } from 'react';

class TodoStatusControl extends Component {

    getId(){
        return `toogle-done-${this.props.id}`;
    }

    checked(){
        return this.props.done ? 'checked':'';
    }

    render() { 
        return ( 
            <div className="row">
                <div className="col-sm">
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id={this.getId()} defaultChecked={this.checked()} onChange={this.props.onStatusChange} />
                        <label className="form-check-label" htmlFor={this.getId()} >Compleate</label>
                    </div>
                </div>
                <div className="col-sm">
                    <button className="btn btn-danger btn-sm" onClick={this.props.onDelete}>Delete</button>
                </div>
            </div>  
       );
    }
}
 
export default TodoStatusControl;