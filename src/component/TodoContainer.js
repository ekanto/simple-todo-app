import React, {Component} from 'react';
import TodoItem from './TodoItem.js';
import uuid from 'uuid';
import withModal from './partial/Modal.js';
import NewTodo from './partial/NewTodo.js';

class TodoContainer extends Component {
    state = { 
        todos: [
            {id: uuid.v4(), title: 'Clean trast', subtitle: 'important', text: 'make sure to finish before launch' , done: true},
            {id: uuid.v4(), title: 'Do homeworks', subtitle: 'important', text: 'make sure to finish before launch' , done: false},
            {id: uuid.v4(), title: 'Hangin on', subtitle: 'important', text: 'make sure to finish before launch' , done: false}
        ]
    }

    handleStatusChange = (id) => {
        this.setState({todos: this.state.todos.map( todo => {
            if( todo.id === id ){
                todo.done = !todo.done;
            }
            return todo;
        } )})
    }

    handleDelete = (id) => {
        this.setState({todos: [...this.state.todos.filter( todo => todo.id !== id )]})
    }

    render() { 
        return this.state.todos.map( (todo) => (
            <TodoItem key={todo.id} todo={todo} onStatusChange={this.handleStatusChange} onDelete={this.handleDelete}/>
        ) );
    }
}

const TodoList = withModal(TodoContainer, NewTodo, 'Add new todo');

export default TodoList;