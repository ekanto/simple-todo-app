import React, { Component } from 'react';
import Nav from "./layout/Nav.js";
import TodoList from "./TodoContainer.js";

class Todo extends Component {
    state = { 
        showAddModal: false
    }

    handleShowAddModal = () => {
        this.setState( {showAddModal: true} );
    }

    handleCloseModal = () => {
        this.setState( {showAddModal: false} );
    }

    render() { 
        return (
            <div className="App">
                <Nav onOpenAddModal={this.handleShowAddModal} />
                <div className="container">
                    <TodoList showModal={this.state.showAddModal} onCloseAddModal={this.handleCloseModal} />
                </div>
            </div>
        );
    }
}
 
export default Todo;