import React, { Component } from 'react';
import TodoStatus from './TodoStatus.js';
import TodoStatusControl from './TodoStatusControl.js';

class TodoItem extends Component {
    render() { 
        const {id, title, subtitle, text, done} = this.props.todo;
        return ( 
        <div className="card m-2">
        <div className="card-body">
          <h5 className="card-title">{title} <TodoStatus done={done} />  </h5>
          <h6 className="card-subtitle mb-2 text-muted">{subtitle}</h6>
          <p className="card-text">{text}</p>
        </div>
        <div className="card-footer">
            <TodoStatusControl id={id} done={done} onStatusChange={this.props.onStatusChange.bind(this, id)} onDelete={this.props.onDelete.bind(this, id)}/>
        </div>
      </div> 
      );
    }
}
 
export default TodoItem;