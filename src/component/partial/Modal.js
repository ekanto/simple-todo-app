import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal.js';

const withModal = (Wrapper, ModalBody, modalTitle = 'no title set') => {
    return class extends React.Component {

        render() {
            const body = ModalBody ? <ModalBody />: 'no body set';
            return (
            <React.Fragment>
                <Wrapper {...this.props} />
                <Modal show={this.props.showModal} onHide={this.props.onCloseAddModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>{modalTitle}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{body}</Modal.Body>
                </Modal>
            </React.Fragment>
            );
        }
    }
}

export default withModal;