import React, { Component } from 'react';
import uuid from 'uuid';

class NewTodo extends Component {
    state = {  
        id: uuid.v4(),
        title: '',
        subtitle: '',
        text: '',
        done: false
    }

    save = (e) => {
        e.preventDefault();
    }

    render() { 
        return (  
            <form onSubmit={this.save}>
                <div className="form-group">
                    <label>Title</label>
                    <input type="text" className="form-control" aria-describedby="title" placeholder="Enter title" name="title" />
                </div>
                <div className="form-group">
                    <label>subtitle</label>
                    <input type="text" className="form-control" aria-describedby="subtitle" placeholder="Enter subtitle" name="subtitle"/>
                </div>
                <div className="form-group">
                    <label>content</label>
                    <textarea className="form-control" aria-describedby="subtitle" placeholder="Enter subtitle" name="text"></textarea>
                </div>
                <button type="submit" className="btn btn-primary btn-sm">Save</button>
            </form>
        );
    }
}
 
export default NewTodo;