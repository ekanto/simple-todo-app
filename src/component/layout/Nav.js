import React, { Component } from 'react';

class Nav extends Component {

    render() { 
        return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">todo list app</a>
            <button className="btn btn-outline-success" type="button" onClick={this.props.onOpenAddModal}>Add</button>
        </nav> 
        );
    }
}
 
export default Nav;