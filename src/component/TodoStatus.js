import React, { Component } from 'react';

class TodoStatus extends Component {

    getClassNames(){
        return `badge badge-pill badge-${this.props.done ? 'success':'primary'}`;
    }

    getText(){
        return this.props.done ? 'done': 'open';
    }

    render() { 
        return ( <span className={this.getClassNames()}>{this.getText()}</span> );
    }
}
 
export default TodoStatus;